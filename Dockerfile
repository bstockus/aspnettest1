FROM microsoft/aspnet:1.0.0-beta4

RUN apt-get update

RUN apt-get install -y git

ADD ./init.sh /app/init.sh
WORKDIR /app

EXPOSE 5004
ENTRYPOINT ["/bin/bash", "init.sh"]
