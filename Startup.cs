using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Diagnostics;
using Microsoft.AspNet.Diagnostics.Entity;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.ConfigurationModel;
using Microsoft.Framework.Logging;
using System.Data.Entity;
using MySql.Data.Entity;

using HelloMvc6.Models;

namespace HelloMvc6 {
    public class Startup {
        
        public IConfiguration Configuration { get; set; }
        
        public Startup(IHostingEnvironment env) {
            
            Configuration = new Configuration().AddJsonFile("config.json").AddEnvironmentVariables();
            
        }

        // This method gets called by a runtime.
        // Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services) {
            
            services.AddTransient<ExampleContext>((a) => ExampleContextFactory.GetContext());
            
            services.AddMvc();
            // Uncomment the following line to add Web API services which makes it easier to port Web API 2 controllers.
            // You will also need to add the Microsoft.AspNet.Mvc.WebApiCompatShim package to the 'dependencies' section of project.json.
            // services.AddWebApiConventions();
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            // Configure the HTTP request pipeline.
            //app.UseWelcomePage();
            
            loggerFactory.AddConsole(LogLevel.Information);
            
            app.UseStaticFiles();
            
            app.UseErrorPage(ErrorPageOptions.ShowAll);
            app.UseDatabaseErrorPage(DatabaseErrorPageOptions.ShowAll);

            // Add MVC to the request pipeline.
            // Add the following route for porting Web API 2 controllers.
            // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
            
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller}/{action}",
                    defaults: new { action = "Index" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Home", action = "Index" });

                routes.MapRoute(
                    name: "api",
                    template: "{controller}/{id?}");
            });
            
            var db = app.ApplicationServices.GetService<ExampleContext>();
            
            db.Database.CreateIfNotExists();
            
            db.SaveChanges();
            
        }
    }
}
