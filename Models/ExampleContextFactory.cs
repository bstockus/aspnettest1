using System;
using MySql.Data.MySqlClient;

namespace HelloMvc6.Models {
	
	public static class ExampleContextFactory {
    	public static ExampleContext GetContext() {
			System.Diagnostics.Debug.WriteLine("ExampleContextFactory::GetContext()");
			string connectionString;
			try {
				connectionString = "server=" + Environment.GetEnvironmentVariable("MYSQL_PORT_3306_TCP_ADDR") + ";port=" +
					Environment.GetEnvironmentVariable("MYSQL_PORT_3306_TCP_PORT") + ";database=Example;uid=root;password=" +
					Environment.GetEnvironmentVariable("MYSQL_ENV_MYSQL_ROOT_PASSWORD") + ";";
			} catch (ArgumentNullException e) {
				connectionString = "server=192.168.59.103;port=3306;database=Example;uid=root;password=pass";
			}
			System.Diagnostics.Debug.WriteLine(connectionString);
        	return new ExampleContext(new MySqlConnection(connectionString), true);
    	}

	}
	
}

