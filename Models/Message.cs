using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.ModelBinding;

namespace HelloMvc6.Models {
	
	public class Message {
		
		[Key]
		public int MessageId { get; set; }
		
		public string Text { get; set; }
		
	}
	
}