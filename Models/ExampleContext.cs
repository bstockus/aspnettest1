
using MySql.Data.Entity;
using System.Data.Common;
using System.Data.Entity;

namespace HelloMvc6.Models {
	
	[DbConfigurationType(typeof(MySqlEFConfiguration))]
	public class ExampleContext : DbContext {
		
		public DbSet<Message> Messages { get; set; }
		
		public ExampleContext(DbConnection existingConnection, bool contextOwnsConnection):base(existingConnection, contextOwnsConnection) {
			
		}
		
		protected override void OnModelCreating(DbModelBuilder builder) {
			
			base.OnModelCreating(builder);
		}
		
	}
	
	
	
}