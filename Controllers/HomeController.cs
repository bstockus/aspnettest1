using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.Caching.Memory;
using HelloMvc6.Models;

namespace HelloMvc6.Controllers {
	
	public class HomeController : Controller {
		
		[FromServices]
		public ExampleContext DbContext { get; set; }
		
		[HttpGet]
		public IActionResult Index() {
			
			return View("~/Views/Home/Index.cshtml", DbContext.Messages.ToList());
			
		}
		
		public IActionResult Create() {
			return View();
		}
		
		[HttpPost]
		public IActionResult Create(Message message) {
			if (ModelState.IsValid) {
				DbContext.Messages.Add(message);
				DbContext.SaveChanges();
				
				return RedirectToAction("Index");
			} else {
				return View(message);
			}
		}
		
	}
	
}